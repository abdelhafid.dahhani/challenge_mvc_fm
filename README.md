# Chat Project: 
* URL in localhost is: http://localhost/chemin-to->/Challenge_MVC_FM/

## Getting Started

These instructions will get you a copy of the project up 
and running on your local machine for development and testing 
purposes.

### Prerequisites

This project is based on > PHP5

## Architecture

I am using the clean architecture invented by the Software engineer, public speaker and  co-author 
of the Agile Manifesto **Robert Cecil Martin** (colloquially known as Uncle Bob)

* [Clean architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)

### Installing

* Clone the project in your workspace and execute the script with the project: db_chat_dev.sql

## Tree

* Minimized
![alt text](https://gitlab.com/abdelhafid.dahhani/challenge_mvc_fm/raw/master/see_this/tree/minized.png)

* Maximized
![alt text](https://gitlab.com/abdelhafid.dahhani/challenge_mvc_fm/raw/master/see_this/tree/maximized.png)

## Databases schema

![alt text](https://gitlab.com/abdelhafid.dahhani/challenge_mvc_fm/raw/master/see_this/screen/database_schema.png)

## Screen shoot of the front end

* NB: Because there is no time enough, I used just a simple js and jquery

![alt text](https://gitlab.com/abdelhafid.dahhani/challenge_mvc_fm/raw/master/see_this/screen/chat.png)

![alt text](https://gitlab.com/abdelhafid.dahhani/challenge_mvc_fm/raw/master/see_this/screen/registration.png)

![alt text](https://gitlab.com/abdelhafid.dahhani/challenge_mvc_fm/raw/master/see_this/screen/registration.png)

## Versioning

Stable Versions available is 1.0.

### License

This project is licensed under the MIT License

## Authors

* **Abdelhafid DAHHANI** <br>
* **Phone: 212611596210** <br>
*Initial work* - Company: HiitConsulting* <br>
