$(document).ready(function() {
    function verifier(param) {
        let input = document.getElementById(param);
        function funcRegex() {
            if (!/[!|?]/.test(input.value)) {
                return true;
            } else {
                return false;
            }
        }
        if (input.value.length > 0 && funcRegex()) {
            $('._' + param).removeClass('has-error');
            $('._' + param + ' span').remove();
            $('._' + param).addClass("has-success has-feedback");
            $('._' + param).append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
            return true
        }
        else if (input.value.length == 0 || /[!|?|]/.test(input.value)) {
            $('._' + param).removeClass('has-success');
            $('._' + param + ' span').remove();
            $('._' + param).addClass('has-error has-feedback');
            $('._' + param).append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            return false
        }
    }

    function focusBlur(param){
        $('.' + param).focus(function () {
            verifier(param);
        });
        $('.' + param).blur(function () {
            verifier(param);
        });
    }

    function returnError (response, idSpan)
    {
        object = JSON.parse(response.responseText);
        $('.' + idSpan).text(object.final);
        $('.' + idSpan).show("slow").delay(2000).hide("slow");
        return true;
    }

    $('#formLogin').submit(function (e) {
        e.preventDefault();
        if (verifier('Pseudo') && verifier('Password')) {
            $.ajax({
                url: $('#formLogin').attr('action'),
                method: "POST",
                data: $('#formLogin').serialize(),
                statusCode: {
                    200: function (response) {
                        $('.SpanSuccess').text(response.final);
                        $('.SpanSuccess').show('slow').delay(2000).hide('slow', function () {
                            sessionStorage.setItem('id', response.id);
                            $(location).attr('href', '?c=Chat&a=chat')
                        });
                    },
                    404: function (response) {
                        returnError(response, 'SpanError');
                    },
                    403: function (response) {
                        returnError(response, 'SpanError');
                    },
                    500: function (response) {
                        $('.SpanError').text(response['final']);
                        $('.SpanError').show('slow').delay(2000).hide('slow');
                    },
                },
                dataType: 'json'
            });
        } else {
            $('.SpanError').text('Please complete all inputs');
            $('.SpanError').show('slow').delay(2000).hide('slow');
        }
    });

    /**
     * Call methods
     */
    focusBlur('Pseudo');
    focusBlur('Password');
});