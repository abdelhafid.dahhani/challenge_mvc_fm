$(document).ready(function() {
    window.socket = webSocketServer();
    userConnected(sessionStorage.getItem('id'), socket);
    function getConnected() {
        $.ajax({
            url: '?c=Login&a=connected',
            method: "GET",
            data: {'id': sessionStorage.getItem('id')},
            statusCode: {
                200: function (response) {
                    $('aside').empty();
                    let user = response['connected'];
                    if (user.length)
                    {
                        for (let i = 0; i < user.length; i++) {
                            $('aside').append('<div class="userConnected" data-name="' + user[i].name +  '" data-id="' + user[i].idU + '">\n' +
                                '                <span class="glyphicon glyphicon-check"></span> ' + user[i].name +
                                '              </div>');
                        }
                    }
                },
                404: function (response) {
                    $('aside').empty();
                    $('aside').append('<div class="userNoConnected">' +
                        '                <span class="glyphicon glyphicon-eye-close"></span> ' + returnError(response).final + '</strong>' +
                        '              </div>');
                },
                500: function (response) {
                    $('aside').empty();
                    $('aside').append('<div class="userNoConnected">' +
                        '                <span class="glyphicon glyphicon-eye-close"></span> ' + returnError(response).final + '</strong>' +
                        '              </div>');
                },
            },
            dataType: 'json'
        });
    }

    function message() {
        let idU_Receiver = $(this).data('id');
        $(this).on( "keydown", function(event) {
            if(event.which == 13) {
                let message = $(this).val();
                if (is_empty(message)){
                    $.ajax({
                        url: $('#formChat').attr('action'),
                        method: "POST",
                        data: {
                            'message': message,
                            'idU_Receiver': idU_Receiver,
                            'idU_Transmitter': sessionStorage.getItem('id')
                        },
                        statusCode: {
                            201: function (response) {
                                $('.notFound').remove();
                                $('body .chatWindow div.message').append(
                                    '                <div style="clear: both;"></div>' +
                                    '                <span style="float: right">' + message + '</span>'
                                );
                                sendMessage(idU_Receiver, message, window.socket);
                                autoScroll();
                            },
                            500: function (response) {

                            },
                        },
                        dataType: 'json'
                    });
                }
                $(this).val('');
                $(this).val().replace(/\n|\r|(\n\r)/g,' ');
            }
        });
    }

    function getMessage(idU_Receiver) {
        $.ajax({
            url: '?c=Chat&a=message',
            method: "GET",
            data: {
                'idU_Receiver': idU_Receiver,
                'idU_Transmitter': sessionStorage.getItem('id')
            },
            statusCode: {
                200: function (response) {
                    let messages = response.messages;
                    for (let i = 0; i < messages.length; i++) {
                        if (sessionStorage.getItem('id') == messages[i].idU_Transmitter) {
                            $('body .chatWindow div.message').append(
                                '                <div style="clear: both;"></div>' +
                                '                <span style="float: right">' + messages[i].message + '</span>'
                            );
                        }else{
                            $('body .chatWindow div.message').append(
                                '                <div style="clear: both;"></div>' +
                                '                <span style="float: left">' + messages[i].message + '</span>'
                            );
                        }
                    }
                    autoScroll();
                },
                500: function (response) {
                    let error = returnError(response).final;
                    $('body .chatWindow div.message').append('<div class="notFound">' +
                        '                <div style="clear: both;"></div>' +
                        '                <span style="text-align: center;background: wheat;">' + error + '</span>'
                    );
                },
            },
            dataType: 'json'
        });
    }

    function autoScroll() {
        let messageGlob = $('div.messageGlob');
        let messageDiv = $('div.message');
        messageGlob.scrollTop(messageDiv.outerHeight());
    }

    function is_empty(message) {
        message = message.replace(/^\s|\n|\r/g,'');
        message = message.replace(/[!|?|]/, '');
        if (message.length > 0 && !/^\s|[!|?|]/.test(message)){
            return true
        }
        return false;
    }

    function closeChat() {
        $('.chatWindow').remove();
    }

    function returnError (response)
    {
        return JSON.parse(response.responseText);
    }

    setInterval(getConnected, 2000);

    $('aside').on('click', '.userConnected', function (e) {
        $('.chatWindow').remove();
        let idU_Receiver = $(this).data('id');
        let name = $(this).data('name');
        $('body').append('<div class="chatWindow">' +
            '            <h4 class="pseudo">' +
            '               ' + name + ' <strong class="close pull-right">x</strong>' +
            '            </h4>' +
            '            <div class="messageGlob">' +
            '               <div class="message">' +
            '               </div>' +
            '            </div>' +
            '            <div class="typo">' +
            '                <form id="formChat" action="?c=Chat&a=sendMessage" method="POST">' +
            '                    <textarea data-id="' + idU_Receiver + '" name="message" id="message" class="message" placeholder="Type your message ...."></textarea>' +
            '                </form>' +
            '            </div>' +
            '        </div>');

        getMessage(idU_Receiver);
        $('.pseudo').on('click', '.close', closeChat);
        $('#formChat').on('focus', '.message', message);
    });


    // Socket :
    function webSocketServer(){
        let websocket_server = new WebSocket("ws://localhost:9300/");
        return websocket_server

    }


    function userConnected(idUser, websocket_server) {
        websocket_server.onopen = function (e) {
            websocket_server.send(
                JSON.stringify({
                    'type': 'socket',
                    'user_id': idUser
                })
            );
        };
    }
    window.socket.onerror = function(e) {

    };

    window.socket.onmessage = function(e)
    {
        let json = JSON.parse(e.responseText);
        switch(json.type) {
            case 'chat':
                $('body .chatWindow div.message').append(
                    '                <div style="clear: both;"></div>' +
                    '                <span style="float: right">' + json.message + '</span>'
                );
                break;
        }
    };

    // Send message
    function sendMessage(idUser, message, websocket_server) {
        websocket_server.send(
            JSON.stringify({
                'type': 'chat',
                'user_id': idUser,
                'chat_msg': message
            })
        );
    }
});