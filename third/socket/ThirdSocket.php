<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/31/18
 * Time: 11:14 PM
 */

include_once 'PHPWebSocket.php';

class ThirdSocket extends PHPWebSocket
{
    protected $server = null;

    public function __construct()
    {
        if ($this->server === null)
            $this->server = new PHPWebSocket();
    }


    function getServer(){
        return $this->server;
    }
    function wsOnMessage($clientID, $message, $messageLength, $binary) {
        $ip = long2ip( $this->server->wsClients[$clientID][6] );
        // check if message length is 0
        if ($messageLength == 0) {
            $this->server->wsClose($clientID);
            return;
        }
        //The speaker is the only person in the room. Don't let them feel lonely.
        if ( sizeof($this->server->wsClients) == 1 )
            $this->server->wsSend($clientID, "There isn't anyone else in the room, but I'll still listen to you. --Your Trusty Server");
        else
            //Send the message to everyone but the person who said it
            foreach ( $this->server->wsClients as $id => $client )
                if ( $id != $clientID )
                    $this->server->wsSend($id, "Visitor $clientID ($ip) said \"$message\"");
    }

    function wsOnOpen($clientID)
    {
        $ip = long2ip( $this->server->wsClients[$clientID][6] );
        $this->server->log( "$ip ($clientID) has connected." );
        //Send a join notice to everyone but the person who joined
        foreach ( $this->server->wsClients as $id => $client )
            if ( $id != $clientID )
                $this->server->wsSend($id, "Visitor $clientID ($ip) has joined the room.");
    }

    function wsOnClose($clientID, $status) {
        $ip = long2ip( $this->server->wsClients[$clientID][6] );
        $this->server->log( "$ip ($clientID) has disconnected." );
        //Send a user left notice to everyone in the room
        foreach ( $this->server->wsClients as $id => $client )
            $this->server->wsSend($id, "Visitor $clientID ($ip) has left the room.");
    }

    function binding(){
        $this->server->bind('message', 'wsOnMessage');
        $this->server->bind('open', 'wsOnOpen');
        $this->server->bind('close', 'wsOnClose');
        return $this->getServer();
    }

}