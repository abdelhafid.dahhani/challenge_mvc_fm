<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 2:12 AM
 *
    ================================================================

                    This is the base class model

    ================================================================
 */

class Model{

    protected $db;
    protected $table;
    protected $fields = array();

    public function __construct($table){
        $dbConfig['host'] = $GLOBALS['config']['config']['host'];
        $dbConfig['user'] = $GLOBALS['config']['config']['user'];
        $dbConfig['password'] = $GLOBALS['config']['config']['password'];
        $dbConfig['databaseName'] = $GLOBALS['config']['config']['databaseName'];
        $dbConfig['port'] = $GLOBALS['config']['config']['port'];
        $dbConfig['collation'] = $GLOBALS['config']['config']['collation'];

        $this->db = new Mysql($dbConfig);
        $this->table = $table;
        $this->getFields();

    }

    /**
     * Get the list of table fields
     */
    private function getFields(){
        $sql = "DESC ". $this->table;
        $result = $this->db->getAll($sql);
        foreach ($result as $v) {
            $this->fields[$v['Field']] = $v['Field'];
            if ($v['Key'] == 'PRI') {
                # If there is PK, save it in $pk variable
                $pk = $v['Field'];
            }elseif ($v['Key'] == 'MUL'){
                $fk = $v['Field'];
            }
        }

        # If there is PK, add it into fields list
        if (isset($pk)) {
            $this->fields['pk'] = $pk;
        }

        # If there is FK, add it into fields list
        if (isset($fk)) {
            $this->fields['fk'] = $fk;
        }
    }

    /**
     * Insert records
     * @access public
     * @param $list
     * @return bool|int
     */
    protected function insert_list($list){
        $field_list = '';  # field list string
        $value_list = '';  # value list string
        foreach ($list as $k => $v) {
            if (in_array($k, $this->fields)) {
                $field_list .= "`".$k."`" . ',';
                $value_list .= "'".$v."'" . ',';
            }
        }

        $field_list .= "`date`" . ',';
        $value_list .= "'".date('y-m-d')."'" . ',';

        # Trim the comma on the right
        $field_list = rtrim($field_list,',');
        $value_list = rtrim($value_list,',');

        # Construct sql statement
        $sql = "INSERT INTO `{$this->table}` ({$field_list}) VALUES ($value_list)";

        if ($this->db->query($sql)) {
            # insert succeed, return the last record’s id
            return $this->db->getInsertId();
        } else {
            # Insert fail, return false
            return false;
        }
    }

    /**
     * Insert records connected
     * @access public
     * @param $object
     * @return bool|int
     */
    protected function insert_object_connected($object){
        $fields = '';
        $values = '';

        $fields .= "`date`" . ',';
        $values .= "'".date('y-m-d')."'" . ',';
        $fields .= "`idU`" . ',';
        $values .= "'".$object->getId()."'" . ',';

        # Trim the comma on the right
        $fields = rtrim($fields,',');
        $values = rtrim($values,',');

        $sql = "INSERT INTO `{$this->table}` ({$fields}) VALUES ($values)";
        if ($this->db->query($sql)) {
            return $this->db->getInsertId();
        } else {
            return false;
        }
    }

    /**
     * Insert records chat
     * @access public
     * @param $object
     * @return bool|int
     */
    protected function insert_object_chat($object){
        $fields = '';
        $values = '';

        $fields .= "`date`" . ',';
        $values .= "'".date('y-m-d')."'" . ',';
        $fields .= "`idU_Transmitter`" . ',';
        $values .= "'".$object->getIdUTransmitter()."'" . ',';
        $fields .= "`idU_Receiver`" . ',';
        $values .= "'".$object->getIdUReceiver()."'" . ',';
        $fields .= "`message`" . ',';
        $values .= "'".$object->getMessage()."'" . ',';

        $fields = rtrim($fields,',');
        $values = rtrim($values,',');

        $sql = "INSERT INTO `{$this->table}` ({$fields}) VALUES ($values)";
        if ($this->db->query($sql)) {
            return $this->db->getInsertId();
        } else {
            return false;
        }
    }

    /**
     * Get info based on PK
     * @param $pk int Primary Key
     * @return array of array of single record
     */
    protected function selectByPk($pk)
    {
        $sql = "select * from `{$this->table}` where `{$this->fields['pk']}`=$pk";
        return $this->db->getRow($sql);
    }

    /**
     * Get info based on PK
     * @param $fk int Foreign Key
     * @return array of array of single record
     */
    protected function selectByFk($fk)
    {
        $sql = "select * from `{$this->table}` where `{$this->fields['fk']}`=$fk";
        return $this->db->getRow($sql);
    }


    /**
     * Get info based on pseudo
     * @param $pseudo string
     * @return array of array of single record
     */
    protected function selectByPseudo($pseudo)
    {
        $sql = "select * from `{$this->table}` where `{$this->fields['pseudo']}`='{$pseudo}'";
        return $this->db->getRow($sql);
    }


    /**
     * Get the count of all records
     * @return bool
     */
    protected function total(){
        $sql = "select count(*) from {$this->table}";
        return $this->db->getOne($sql);
    }
}