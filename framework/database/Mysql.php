<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 1:54 AM
 *
    ================================================================

      (Class) Encapsulate Database connection and some basic query

    ================================================================
 */

class Mysql{
    const DEFAULT_PORT = 3306;
    const DEFAULT_COLLATION = 'utf8mb4_unicode_ci';

    protected $conn = false;
    protected $sql;

    /**
     * Constructor used to connect to the database, select database and set charset
     * Mysql constructor.
     * @param array $config
     */
    public function __construct($config = array()){
        $host = isset($config['host'])? $config['host'] : 'localhost';
        $user = isset($config['user'])? $config['user'] : 'root';
        $password = isset($config['password'])? $config['password'] : '';
        $port = isset($config['port'])? $config['port'] : self::DEFAULT_PORT;
        $databaseName = isset($config['databaseName'])? $config['databaseName'] : '';
        $collation = isset($config['collation'])? $config['collation'] : self::DEFAULT_COLLATION;
        $this->conn = new PDO("mysql:host=$host:$port;dbname=$databaseName",$user,$password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        # $this->setCollation($collation);
    }

    /**
     * Set collation
     * @access private
     * @param $collation
     */
    private function setCollation($collation){
        $sql = 'set names '.$collation;
        $this->query($sql);
    }

    /**
     * Execite an sql statement
     * @param $sql
     * @return resource
     */
    public function query($sql){
        $this->sql = $sql;
        $str = $sql . "  [". date("Y-m-d H:i:s") ."]" . PHP_EOL;
        #file_put_contents("/framework/var/logs/log.txt", $str,FILE_APPEND);
        $result = $this->conn->query($this->sql);

        if (!$result) {
            die($this->errno().':'.$this->error().'<br />Error SQL statement is '.$this->sql.'<br />');
        }
        return $result;
    }

    /**
     * Get the first column of the first row
     * @access public
     * @param $sql
     * @return bool
     */
    public function getOne($sql){
        $result = $this->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $row[0];
        } else {
            return false;
        }
    }

    /**
     * Get one record
     * @access public
     * @param $sql
     * @return array|bool
     */
    public function getRow($sql){
        if ($result = $this->query($sql)) {
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row;
        } else {
            return false;
        }
    }

    /**
     * Get all records
     * @access public
     * @param $sql
     * @return array
     */
    public function getAll($sql){
        $result = $this->query($sql);
        $list = array();
        while ($row = $result->fetch(PDO::FETCH_ASSOC)){
            $list[] = $row;
        }
        return $list;

    }

    /**
     * Get the value of a column
     * @access public
     * @param $sql
     * @return array
     */
    public function getCol($sql){
        $result = $this->query($sql);
        $list = array();
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $list[] = $row[0];
        }
        return $list;
    }

    /**
     * @param $sql
     * @param $params
     * @return array
     */
    public function prepareAndExecute($sql, $params){
        $request = $this->conn->prepare($sql);
        $request->execute($params);
        return $this->fetching($request);
    }

    /**
     * @param $request
     * @return array
     */
    private function fetching($request){
        $rows = [];
        while ($row = $request->fetch(PDO::FETCH_ASSOC)) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * @param $request
     * @return mixed
     */
    private function executing($request){
        return $request->execute();
    }

    /**
     * Get the last insert id
     * @return int
     */
    public function getInsertId(){
        return $this->conn->lastInsertId();
    }

    /**
     * Get error number
     * @access private
     * @return int
     */
    public function errorNo(){
        return mysql_errno($this->conn);
    }

    /**
     * Get error message
     * @access private
     * @return string
     */
    public function error(){
        return mysql_error($this->conn);
    }
}