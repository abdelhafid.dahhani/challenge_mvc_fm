<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 1:24 AM
 */

class Framework
{
    public static function run()
    {
        self::init();
        self::autoload();
        self::dispatch();
    }

    /**
     * Init
     */
    private static function init() {
        # Define the path constant
        define("DS", DIRECTORY_SEPARATOR);
        define("ROOT", getcwd() . DS);
        define("APP_PATH", ROOT . 'application' . DS);
        define("THIRD_PATH", ROOT . 'third' . DS);
        define("FRAMEWORK_PATH", ROOT . "framework" . DS);
        define("PUBLIC_PATH", ROOT . "public" . DS);
        define("CONFIG_PATH", APP_PATH . "config" . DS);
        define("CONTROLLER_PATH", APP_PATH . "controllers" . DS);
        define("MODEL_PATH", APP_PATH . "models" . DS);
        define("REPOSITORIES_PATH", APP_PATH . "repositories" . DS);
        define("SOCKET_PATH", THIRD_PATH . "socket" . DS);
        define("SERVICE_PATH", APP_PATH . "services" . DS);
        define("UTILITIES_PATH", APP_PATH . "utilities" . DS);
        define("VIEW_PATH", APP_PATH . "views" . DS);
        define("CORE_PATH", FRAMEWORK_PATH . "core" . DS);
        define('DB_PATH', FRAMEWORK_PATH . "database" . DS);
        define("LIB_PATH", FRAMEWORK_PATH . "libraries" . DS);
        define("HELPER_PATH", FRAMEWORK_PATH . "helpers" . DS);
        define("UPLOAD_PATH", PUBLIC_PATH . "uploads" . DS);

        # Define platform, controller, action, for example:
        # index.php?p=admin&c=Goods&a=add
        @$page = $_GET['page'];
        @$page = explode('/', $page);
        define("PLATFORM", isset($_REQUEST['p']) ? $_REQUEST['p'] : 'home');
        define("CONTROLLER", isset($_REQUEST['c']) ? $_REQUEST['c'] : 'Registration');
        define("ACTION", isset($_REQUEST['a']) ? $_REQUEST['a'] : 'registration');
        define("CURR_CONTROLLER_PATH", CONTROLLER_PATH . DS);
        define("CURR_VIEW_PATH", VIEW_PATH . DS);

        # Loading core classes
        require CORE_PATH . "Controller.php";
        require CORE_PATH . "Loader.php";
        require DB_PATH . "Mysql.php";
        require DB_PATH . "Model.php";
        #require SOCKET_PATH . "ThirdSocket.php";

        # Loading configuration file
        $GLOBALS['config'] = include CONFIG_PATH . "config.php";
        #$GLOBALS['socket'] = new ThirdSocket();

        # Starting session
        session_start();
    }

    /**
     * Autoloading
     */
    private static function autoload(){
        spl_autoload_register(array(__CLASS__,'load'));
    }

    /**
     * Load method called above ine autoload method inside spl_autoload_register
     * @param $className
     */
    private static function load($className){
        if (substr($className, -10) == 'Controller'){
            require_once CURR_CONTROLLER_PATH . "$className.php";
        } elseif (substr($className, -5) == 'Model'){
            require_once  MODEL_PATH . "$className.php";
        }elseif (substr($className, -7) == 'Service'){
            require_once  SERVICE_PATH . "$className.php";
        }elseif (substr($className, -9) == 'Utilities'){
            require_once  UTILITIES_PATH . "$className.php";
        }elseif (substr($className, -10) == 'Repository'){
            require_once  REPOSITORIES_PATH . "$className.php";
        }elseif (substr($className, -6) == 'Socket'){
            require_once  SOCKET_PATH . "$className.php";
        }
    }

    /**
     * Return and dispatch
     */
    private static function dispatch(){
        # Instantiate the controller class and call its action method
        $controllerName = CONTROLLER . "Controller";
        $actionName = ACTION . "Action";
        $controller = new $controllerName;
        $controller->$actionName();

    }
}