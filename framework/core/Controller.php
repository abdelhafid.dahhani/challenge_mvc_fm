<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 1:42 AM
 */

/**
 * This is the base controller.
 * Class Controller
 */
class Controller{
    // Base Controller has a property called $loader, it is an instance of Loader class(introduced later)
    protected $loader;

    /**
     * It contain a reference to Loader
     * Controller constructor.
     */
    public function __construct(){
        $this->loader = new Loader();
    }

    /**
     * @param $url
     * @param $message
     * @param int $wait
     */
    public function redirect($url, $message, $wait=0){
        if ($wait == 0){
            header("Location:$url");
        } else {
            include CURR_VIEW_PATH . "message.html";
        }
        exit;
    }
}