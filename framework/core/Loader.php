<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 1:52 AM
 */

class Loader{

    /**
     * @param $lib
     */
    public function library($lib){
        include LIB_PATH . "$lib.php";
    }

    /**
     * @param $helper
     */
    public function helper($helper){
        include HELPER_PATH . "{$helper}_helper.php";
    }

}