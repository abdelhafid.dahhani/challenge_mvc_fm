<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 7:22 AM
 */

/**
 * Class UserService
 */
class UserService{

    const CONNECTED = 'connected';

    public function __construct($table)
    {
        $this->userRepository = new UserRepository($table);
        $this->connectedService = new connectedService(self::CONNECTED);
    }

    public function getUsers(){
        $users = $this->userRepository->getUser();
        foreach ($users as $user) {
            $u = [];
            $u[] = new UserModel(
                array(
                    'name' => $user['name'],
                    'pseudo' => $user['pseudo'],
                    'date' => $user['date'],
                )
            );
        }
        return $u;
    }

    public function registration($params){
        /**
         * @var $name
         * @var $pseudo
         * @var $password
         */
        extract($params);
        $ar = [];
        $user = $this->userRepository->selectByPseudo($pseudo);
        if (!$user){
            $user = new UserModel(
                array(
                    'name' => $name,
                    'pseudo' => $pseudo,
                    'password' => $password,
                    'date' => date('d-m-y')
                )
            );
            $response = $this->userRepository->registration($params);
            $ar['id'] = $response;
            $ar['final'] = 'success';
            return $ar;
        }
        $ar['final'] = 'collision pseudo already exist';
        return $ar;
    }

    public function login($params){
        /**
         * @var $pseudo
         * @var $password
         */
        extract($params);
        $ar = [];
        $user = $this->userRepository->selectByPseudo($pseudo);
        if ($user){
            $user = new UserModel($user);
            if ($user->getPassword() == $password) {
                $ar['id'] = $user->getId();
                $ar['final'] = 'authenticated';
                $_SESSION["id"] = $ar['id'];
                $this->connectedService->addConnected($user);
                return $ar;
            }
            $ar['final'] = 'error password';
            return $ar;
        }
        $ar['final'] = 'account not found';
        return $ar;
    }
}