<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/31/18
 * Time: 14:53 AM
 */

/**
 * Class ChatService
 */
class ChatService{

    const USER = 'user';

    public function __construct($table)
    {
        $this->chatRepository = new ChatRepository($table);
        $this->userRepository = new UserRepository(self::USER);
    }

    public function insertMessage($params){
        /**
         * @var $idU_Transmitter
         * @var $idU_Receiver
         * @var $message
         */
        extract($params);

        $ar = [];
        $ar['final'] = 500;
        $userTransmitter = $this->userRepository->selectByPk($idU_Transmitter);
        $userReceiver = $this->userRepository->selectByPk($idU_Receiver);
        if ($userTransmitter and $userReceiver){
            $chat = new ChatModel(array(
                'idU_Transmitter' => $idU_Transmitter,
                'idU_Receiver' => $idU_Receiver,
                'message' => $message
            ));
            $response = $this->chatRepository->insertMessage($chat);
            if (!$response)
                return $ar;
            $ar['final'] = 201;
            return $ar;

        }
        return $ar;
    }

    /**
     * @param $params
     * @return array
     */
    public function getMessage($params)
    {
        $ar = [];
        if (!$params) {
            $ar['final'] = 'you have no session in our server';
            return $ar;
        }
        $rows = $this->chatRepository->getMessage($params);
        if (!empty($rows)) {
            $ar['final'] = 'success';
            $ar['messages'] = $rows;
            return $ar;
        }
        $ar['final'] = 'No message found';
        return  $ar;
    }
}