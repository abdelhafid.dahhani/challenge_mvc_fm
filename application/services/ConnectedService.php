<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 9:01 AM
 */

/**
 * Class ConnectedService
 */
class ConnectedService{

    public function __construct($table)
    {
        $this->connectedRepository = new ConnectedRepository($table);
    }

    /**
     * @param Object $object
     * @return int
     */
    public function addConnected($object)
    {
        if ($object) {
            return $this->connectedRepository->addConnected($object);
        }else{
            return false;
        }
    }

    /**
     * @param $params
     * @return array
     */
    public function getConnected($params)
    {
        $ar = [];
        $ar['connected'] = [];
        if (!$params) {
            $ar['final'] = 'you have no session in our server';
            return $ar;
        }
        $rows = $this->connectedRepository->getConnected($params);
        if (!empty($rows)) {
            $ar['final'] = 'success';
            $ar['connected'] = $rows;
            return $ar;
        }
        $ar['final'] = 'No user connected';
        return  $ar;
    }
}


