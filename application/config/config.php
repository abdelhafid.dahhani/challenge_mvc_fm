<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 2:45 AM
 */

$config['host'] = "localhost";
$config['user'] = 'root';
$config['password'] = '0631756801';
$config['port'] = '3307';
$config['databaseName'] = 'db_chat_dev';
$config['collation'] = 'utf8mb4_unicode_ci';


$http["_ok"] = 200;
$http["_created"] = 201;
$http["_no_content"] = 204;
$http["_bad_request"] = 400;
# This code(401) used just in refresh token or token expired
$http["_unauthorized"] = 401;
# This code(403) used in case code reset expired or incorrect / Login Failed
$http["_forbidden"] = 403;
$http["_not_found"] = 404;
$http["_collision"] = 409;
$http["_unsupported_media_type"] = 415;
$http["_unprocessable"] = 422;
$http["_internal_server_error"] = 500;
$http["_service_unavailable"] = 503;
$http["_phone_not_found"] = 402;
$http["_device_not_found"] = 407;

return array('config' => $config, 'http' => $http);