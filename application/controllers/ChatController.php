<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 2:31 AM
 */

/**
 * Class ChatController
 */
class ChatController extends Controller{

    public function chatAction(){
        if (!isset($_SESSION["id"]) and empty($_SESSION["id"])){
            include CURR_VIEW_PATH . "login.html";
            return true;
        }
        include  CURR_VIEW_PATH . "chat.html";
    }

    public function sendMessageAction(){
        $idU_Transmitter = $_POST['idU_Transmitter'];
        $idU_Receiver = $_POST['idU_Receiver'];
        $message = $_POST['message'];
        $ar = [];

        if (!isset($_SESSION["id"]) and empty($_SESSION["id"]) and $idU_Transmitter != $_SESSION["id"]){
            session_destroy();
            httpCodeUtilities::http('_internal_server_error');
            $ar['final'] = 'you have no session in our server';
            return JsonUtilities::encode($ar);
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $params = ['idU_Transmitter' => $idU_Transmitter, 'idU_Receiver' => $idU_Receiver, 'message' => $message];
            $chatService = new ChatService('chat');
            $response = $chatService->insertMessage($params);
            switch ($response) {
                case $response['final'] == 201:
                    httpCodeUtilities::http('_created');
                    break;
                default:
                    httpCodeUtilities::http('_internal_server_error');
            }
            return JsonUtilities::encode($response);
        }
    }

    public function messageAction(){
        $idU_Transmitter = $_GET['idU_Transmitter'];
        $idU_Receiver = $_GET['idU_Receiver'];

        $ar = [];
        if (!isset($_SESSION["id"]) or empty($_SESSION["id"]) or $idU_Transmitter != $_SESSION["id"]){
            session_destroy();
            httpCodeUtilities::http('_internal_server_error');
            $ar['final'] = 'you have no session in our server';
            return JsonUtilities::encode($ar);
        }
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $chatService = new ChatService('chat');
            $params = ['idU_Transmitter' => $idU_Transmitter, 'idU_Receiver' => $idU_Receiver ];
            $response = $chatService->getMessage($params);
            switch ($response) {
                case $response['final'] == 'No message':
                    httpCodeUtilities::http('_not_found');
                    break;
                case $response['final'] == 'success':
                    httpCodeUtilities::http('_ok');
                    break;
                default:
                    httpCodeUtilities::http('_internal_server_error');
            }
            return JsonUtilities::encode($response);
        }
    }
}