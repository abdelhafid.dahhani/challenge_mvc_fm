<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 2:31 AM
 */

/**
 * Class IndexController
 */
class RegistrationController extends Controller{

    public function registrationAction(){
        if (isset($_SESSION["id"]) and !empty($_SESSION["id"])){
            include CURR_VIEW_PATH . "chat.html";
            return true;
        }
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $params['name'] = htmlspecialchars(($_POST["Name"]), ENT_QUOTES | ENT_HTML5);
            $params['pseudo'] = htmlspecialchars(($_POST["Pseudo"]), ENT_QUOTES | ENT_HTML5);
            $params['password'] = htmlspecialchars(($_POST["Password"]), ENT_QUOTES | ENT_HTML5);
            $userService = new UserService("user");
            $response = $userService->registration($params);
            switch ($response){
                case $response['final'] == 'collision pseudo already exist':
                    httpCodeUtilities::http('_collision');
                    break;
                case $response['final'] == 'success':
                    httpCodeUtilities::http('_created');
                    break;
                default:
                    httpCodeUtilities::http('_internal_server_error');
            }
            return JsonUtilities::encode($response);
        }
        # Load View template views
        include  CURR_VIEW_PATH . "registration.html";
    }
}