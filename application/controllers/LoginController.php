<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 2:31 AM
 */

/**
 * Class LoginController
 */
class LoginController extends Controller{

    public function loginAction()
    {
        if (isset($_SESSION["id"]) and !empty($_SESSION["id"])){
            include CURR_VIEW_PATH . "chat.html";
            return true;
        }
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $params['pseudo'] = htmlspecialchars(($_POST["Pseudo"]), ENT_QUOTES | ENT_HTML5);
            $params['password'] = htmlspecialchars(($_POST["Password"]), ENT_QUOTES | ENT_HTML5);
            $userService = new UserService("user");
            $response = $userService->login($params);
            switch ($response) {
                case $response['final'] == 'account not found':
                    httpCodeUtilities::http('_not_found');
                    break;
                case $response['final'] == 'error password':
                    httpCodeUtilities::http('_forbidden');
                    break;
                case $response['final'] == 'authenticated':
                    httpCodeUtilities::http('_ok');
                    break;
                default:
                    httpCodeUtilities::http('_internal_server_error');
            }
            return JsonUtilities::encode($response);
        }
        # Load View template views
        include CURR_VIEW_PATH . "login.html";
    }

    public function connectedAction(){
        $id = $_GET['id'];
        $ar = [];
        if (!isset($_SESSION["id"]) or empty($_SESSION["id"]) or $id != $_SESSION["id"]){
            session_destroy();
            httpCodeUtilities::http('_internal_server_error');
            $ar['final'] = 'you have no session in our server';
            return JsonUtilities::encode($ar);
        }
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $connectedService = new ConnectedService('connected');
            $params = ['idU' => $id];
            $response = $connectedService->getConnected($params);
            switch ($response) {
                case $response['final'] == 'No user connected':
                    httpCodeUtilities::http('_not_found');
                    break;
                case $response['final'] == 'success':
                    httpCodeUtilities::http('_ok');
                    break;
                default:
                    httpCodeUtilities::http('_internal_server_error');
            }
            return JsonUtilities::encode($response);
        }
    }
}