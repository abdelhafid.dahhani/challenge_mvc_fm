<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 3:50 PM
 */

class httpCodeUtilities
{
    static function http($string){
        http_response_code(self::getCodeHttp($string));
    }

    static protected function getCodeHttp($string){
        return $GLOBALS['config']['http'][$string];
    }
}