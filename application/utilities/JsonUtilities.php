<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 3:23 PM
 */

class JsonUtilities
{
    /**
     * @param $array
     * @return string
     */
    public static function encode($array)
    {
        echo json_encode($array);
    }
}