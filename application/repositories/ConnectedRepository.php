<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 8:55 PM
 */

class ConnectedRepository extends Model
{
    /**
     * @param Object $object
     * @return int
     */
    public function addConnected($object)
    {
        if ($object)
            return parent::insert_object_connected($object);
    }

    /**
     * @param $params
     * @return array
     */
    public function getConnected($params){
        $table_joined = 'user';
        if ($params){
            $sql = "select $table_joined.name, $this->table.* from $this->table, $table_joined WHERE idU!= :idU AND $this->table.idU = $table_joined.id";
            return $this->db->prepareAndExecute($sql,$params);
        }
    }
}
