<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 8:56 PM
 */

class ChatRepository extends Model
{
    /**
     * @param $object
     * @return bool|int
     */
    public function insertMessage($object)
    {
        return parent::insert_object_chat($object);
    }

    /**
     * @param $params
     * @return array
     */
    public function getMessage($params){
        /**
         * @var $idU_Transmitter
         * @var $idU_Receiver
         */
        extract($params);
        if ($params){
            $sql = "select * from $this->table WHERE (idU_Receiver= :idU_Receiver AND idU_Transmitter= :idU_Transmitter) or (idU_Receiver= :idU_Transmitter AND idU_Transmitter= :idU_Receiver)";
            return $this->db->prepareAndExecute($sql,$params);
        }
    }
}