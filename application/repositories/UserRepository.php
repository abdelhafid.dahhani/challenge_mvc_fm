<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 5:11 PM
 */

class UserRepository extends Model{
    /**
     * @return array
     */
    public function getUser(){
        $sql = "select * from $this->table";
        return $this->db->getAll($sql);
    }

    /**
     * @param string $pseudo
     * @return array
     */
    public function selectByPseudo($pseudo)
    {
        return parent::selectByPseudo($pseudo);
    }

    /**
     * @param int $id
     * @return array
     */
    public function selectByPk($id)
    {
        return parent::selectByPk($id);
    }

    /**
     * @param $list
     * @return bool|int
     */
    public function registration($list)
    {
        return $this->insert_list($list);
    }

    public function addConnected($object)
    {
        return parent::insert_object_connected($object);
    }
}