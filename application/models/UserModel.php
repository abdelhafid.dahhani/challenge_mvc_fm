<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 2:29 AM
 */

/**
 * Class UserModel
 */
class UserModel {
    protected $id,
        $name,
        $pseudo,
        $password,
        $date;

    public function __construct($valeur = array())
    {
        if (!empty($valeur))
        {
            $this->hydrate($valeur);
        }
    }

    /**
     * @return mixed
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function setId($id){
        if ($id)
            $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }


    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        if (is_string($password) && !empty($password))
        {
            $this->password = $password;
        }
    }

    public function getDate(){
        return $this->date;
    }

    public function setDate($date){
        if (is_string($date) && !empty($date))
        {
            $this->date = $date;
        }
    }

    public function hydrate($donnees)
    {
        foreach ($donnees as $attribut => $valeur)
        {
            $method = 'set'.ucfirst($attribut);
            if(is_callable(array($this, $method)))
            {
                $this->$method($valeur);
            }
        }
    }
}