<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 8:26 PM
 */

class ConnectedModel
{
    protected $id,
        $date,
        $idU;

    public function __construct($valeur = array())
    {
        if (!empty($valeur))
        {
            $this->hydrate($valeur);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getIdU()
    {
        return $this->idU;
    }

    /**
     * @param mixed $idU
     */
    public function setIdU($idU)
    {
        if (!empty($idU))
        {
            $this->idU = $idU;
        }
    }

    public function hydrate($donnees)
    {
        foreach ($donnees as $attribut => $valeur)
        {
            $method = 'set'.ucfirst($attribut);
            // echo $method;
            if(is_callable(array($this, $method)))
            {
                $this->$method($valeur);
            }
        }
    }

}