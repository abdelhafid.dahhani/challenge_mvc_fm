<?php
/**
 * Created by PhpStorm.
 * User: lifestyle Abdelhafid DAHHANI
 * Date: 10/30/18
 * Time: 8:29 PM
 */

class ChatModel
{
    protected $id,
        $message,
        $date,
        $idU_Transmitter,
        $idU_Receiver;

    public function __construct($valeur = array())
    {
        if (!empty($valeur))
        {
            $this->hydrate($valeur);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */

    public function getIdUTransmitter()
    {
        return $this->idU_Transmitter;
    }

    /**
     * @return mixed
     */
    public function getIdUReceiver()
    {
        return $this->idU_Receiver;
    }

    /**
     * @param mixed $idU_Transmitter
     */
    public function setIdU_Transmitter($idU_Transmitter)
    {
        if (!empty($idU_Transmitter))
        {
            $this->idU_Transmitter = $idU_Transmitter;
        }
    }

    /**
     * @param mixed $idU_Receiver
     */
    public function setIdU_Receiver($idU_Receiver)
    {
        if (!empty($idU_Receiver))
        {
            $this->idU_Receiver= $idU_Receiver;
        }
    }

    public function hydrate($donnees)
    {
        foreach ($donnees as $attribut => $valeur)
        {
            $method = 'set'.ucfirst($attribut);
            // echo $method;
            if(is_callable(array($this, $method)))
            {
                $this->$method($valeur);
            }
        }
    }
}